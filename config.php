<?php
$groupsArray = [
    1 => "PZ-21",
    2 => "PZ-22",
    3 => "PZ-23",
    4 => "PZ-24",
    5 => "PZ-25",
    6 => "PZ-26",
    7 => "PZ-27"
];

$gendersArray = [
    1 => "Male",
    2 => "Female"
];

$servername  = "localhost";
$username = "root";
$password = "root";
$dbname = "students";

try 
{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 
catch (PDOException $e) 
{
    $connError = "Error with connection to DataBase: " . $e->getMessage();
}

function getStudentsFromDB()
{
    global $conn;
    try 
    {
        $stmt = $conn->prepare("SELECT * FROM studentstable");
        $stmt->execute();
        $students = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $students;
    } 
    catch (PDOException $e) 
    {
        $fetchAllError = "Error while fetching table data: " . $e->getMessage();
    }
}

function confirmError($code, $message)
{
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    echo json_encode($response);
    exit;
}