const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    socketId: { type: String, required: true },
    userName: { type: String, required: true }
});
const User = mongoose.model('User', userSchema);

const messageSchema = new mongoose.Schema({
    senderSocketId: { type: String, required: true },
    senderUserName: { type: String, required: true },
    receiverUserName: { type: String, required: true },
    message: { type: String, required: true }
});
const Message = mongoose.model('Message', messageSchema);

const relationSchema = new mongoose.Schema({
    senderId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    receiverId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: false },
    messageId: { type: mongoose.Schema.Types.ObjectId, ref: 'Message', required: true },
});
const UsersRelation = mongoose.model('UsersRelation', relationSchema);

module.exports = {
    User,
    Message,
    UsersRelation
};
