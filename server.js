const http = require('http');
const socketIo = require('socket.io');
const server = http.createServer();
const io = socketIo(server, { cors: { origin: '*', } });

const mongoose = require('mongoose');
const { User, Message, UsersRelation } = require('./models');
const mongoDB = "mongodb"
mongoose.connect(mongoDB)
  .then(() => { console.log('MongoDB connected!'); })
  .catch(err => console.log(err));

const port = 3000;
const connections = []

io.on('connection', (socket) => {
  console.log(`New user connected ${socket.id}`);
  connections.push(socket);

  socket.on('new-user', async name => {
    socket.username = name;
    try {
      await AddOrUpdateUser(socket.id, name);
      const users = await User.find().lean();
      if (!users) {
        console.log('No users in DB');
      }
      const changedUsers = {};
      users.forEach(user => {
        changedUsers[user.socketId] = user.userName;
      });

      socket.broadcast.emit('new-chat', { id: socket.id, name: name, usersList: changedUsers });
      const relations = await UsersRelation.find({ receiverId: null }).lean();
      const messageIds = relations.map(relation => relation.messageId);
      const messages = await Message.find({ _id: { $in: messageIds } }).lean();

      socket.emit('user-connect', {
        id: socket.id,
        chatHistory: messages,
        usersList: changedUsers
      });

    } catch (error) {
      console.error('Error handling new user:', error);
    }
  });

  socket.on('send-message', async data => {
    try {
      const message = new Message(data);
      await message.save();

      const sender = await User.findOne({ userName: data.senderUserName });
      let receiver = null;
      if (data.receiverUserName !== '0') {
        receiver = await User.findOne({ userName: data.receiverUserName });
      }

      const relation = new UsersRelation({
        senderId: sender._id,
        receiverId: receiver ? receiver._id : null,
        messageId: message._id
      });
      await relation.save();

      if (data.receiverUserName == '0') {
        socket.broadcast.emit('message', data);
      } else {
        const targetSocket = connections.find(conn => conn.id === receiver.socketId);
        if (targetSocket) {
          socket.to(receiver.socketId).emit('message', data);
        }
      }
    } catch (error) {
      console.error('Error in sending chat message:', error);
    }
  });

  socket.on('get-chat-history', async (chatName, userName) => {
    try {
      const currentUser = await User.findOne({ userName: userName });
      if (!currentUser) {
        console.error('Current user not found');
        return;
      }
      let messages = [];
  
      if (chatName == '0') {
        const groupRelations = await UsersRelation.find({ receiverId: null }).lean();
        const groupMessageIds = groupRelations.map(relation => relation.messageId);
        messages = await Message.find({ _id: { $in: groupMessageIds } }).lean();
      } else {
        const chatUser = await User.findOne({ userName: chatName });
        if (!chatUser) {
          console.error('User not found');
          return;
        }
        const relations = await UsersRelation.find({
          $or: [
            { senderId: currentUser._id, receiverId: chatUser._id },
            { senderId: chatUser._id, receiverId: currentUser._id }
          ]
        }).lean();
  
        const messageIds = relations.map(relation => relation.messageId);
        messages = await Message.find({ _id: { $in: messageIds } }).lean();
      }
  
      socket.emit('chat-history', messages);
    } catch (error) {
      console.error('Error in getting chat history:', error);
    }
  });

  socket.on('disconnect', () => {
    console.log(`User ${socket.id} disconnected (`);
    const index = connections.indexOf(socket);
    if (index !== -1) {
      connections.splice(index, 1);
    }
  });
});

async function AddOrUpdateUser(socketId, userName) {
  try {
    const existingUser = await User.findOne({ userName });
    if (!existingUser) {
      const user = new User({ socketId, userName });
      await user.save();
    }
    else{
      await User.updateOne({ userName }, { $set: { socketId } });
    }
  } catch (error) {
    console.error('Error in adding or updating user:', error);
  }
}

server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});