<?php
include "config.php";
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['id'])) {
        confirmError(112, "ID is empty!");
    }
    
    $stmt = $conn->prepare("DELETE FROM `studentstable` WHERE id = :id");
    
    $id = $_POST['id'];
    $stmt->bindParam(':id', $id);

    if (!$stmt->execute()) {
        $errorInfo = $stmt->errorInfo();
        confirmError($errorInfo[0], $errorInfo[2]);
    }
    if ($stmt->rowCount() === 0) {
        confirmError(113, "Student wasn't found in DataBase!");
    }

    $response["status"] = true;
    $response["id"] = $id;
    echo json_encode($response);
    exit;
}

http_response_code(403);
echo "Forbidden resourse!";
