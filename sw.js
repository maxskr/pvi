const CACHE_NAME = "site_cache";
const urlsToCache = [
  "./dashboard.php",
  "./index.php",
  "./tasks.php",
  "./style.css",
  "./script.js",
  "./sf.jfif"
];

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      console.log("Opened cache");
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener("fetch", function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request);
    })
  );
})