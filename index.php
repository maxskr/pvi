<!DOCTYPE html>
<html lang="uk">
<!-- alert змінити, слухачі на модальне вікно при чому в кнопок мають бути датаатрибути по яким то робити, group i gender через айді, додати прихований айді при додаванні він пустий, а едіт число. в об'єкт при сабміті присвоювати той айді чи шо -->
<!-- переробити конфіг на з айді, preg/-match забрати, фікс birthday undefined в network, замінити розбиття birthday без parts бо є якісь встроєні способи date чи шо , змінити msg на message, забрати await, не використовувати body при json -->
<!-- переробити status-dot-active, і помилка fetch не виводиться -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>My CMS</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
	<!-- <link rel="manifest" href="manifest.json"> -->
</head>

<body>
	<?php include("config.php"); ?>
	<div class="container-fluid p-0 d-flex h-100 fw-bold">
		<div id="bdSidebar" class="d-flex flex-column flex-shrink-0 bg-secondary text-white offcanvas-md offcanvas-start text-center">
			<a href="#" class="navbar-brand bg-body-secondary text-black p-3">CMS</a>
			<ul class="mynav nav nav-pills flex-column mb-auto p-3">
				<li class="nav-item mb-1">
					<a href="tasks.php">
						Tasks
					</a>
				</li>
				<li class="nav-item mb-1">
					<a href="index.php" class="active">
						Students
					</a>
				</li>
				<li class="nav-item mb-1">
					<a href="dashboard.php">
						Dashboard
					</a>
				</li>
			</ul>
		</div>

		<div class="container-fluid p-0 bg-light">
			<div class="p-3 d-md-none d-flex text-black bg-body-secondary">
				<a href="#" class="text-black" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
					<i class="fa-solid fa-bars"></i>
				</a>
				<span class="ms-3">CMS</span>
			</div>

			<div class="row p-2 m-0 bg-body-secondary idk2">
				<div class="col d-flex align-middle justify-content-end">
					<li class="nav-item dropdown d-block me-2 idk">
						<a class="nav-link dropdown-toggle p-2" href="messages.php" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<span class="notifications">
								<i class="fa-regular fa-bell fa-xl"></i>
								<span class="dot"></span>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="messages.php"><i class="fa-regular fa-user"></i> Message #1
									<span class="user-text">user1</span></a></li>
							<li><a class="dropdown-item" href="messages.php"><i class="fa-regular fa-user"></i> Message #2
									<span class="user-text">user2</span></a></li>
							<li><a class="dropdown-item" href="messages.php"><i class="fa-regular fa-user"></i> Message #3
									<span class="user-text">user3</span></a></li>
						</ul>
					</li>
					<li class="nav-item dropdown d-block idk">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<img src="sf.jfif" class="user-avatar d-inline-block align-text-center">
							<span class="d-none d-sm-inline">Shadow Fiend</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Profile</a></li>
							<li><a class="dropdown-item" href="#">Log out</a></li>
						</ul>
					</li>
				</div>
			</div>

			<div class="container-fluid p-4">
				<div class="row align-items-center">

					<?php if (isset($connError)) : ?>
						<div class="server-message text-danger"><?php echo $connError; ?></div>
					<?php endif; ?>

					<div class="col-9 col-lg-11">
						<h1>Students</h1>
					</div>
					<div class="col-3 col-lg-1 text-end">
						<button type="button" class="action-button rounded-4" data-action-id="" data-bs-toggle="modal" data-bs-target="#addModal">
							<i class="fa-solid fa-plus"></i>
						</button>
					</div>
				</div>
				<div class="table-responsive border border-secondary">
					<table class="table text-nowrap text-center m-0">
						<thead>
							<tr>
								<th class="bg-body-secondary"><input type="checkbox" id="myCheckbox"></th>
								<th class="bg-body-secondary">Group</th>
								<th class="bg-body-secondary">Name</th>
								<th class="bg-body-secondary">Gender</th>
								<th class="bg-body-secondary">Birthday</th>
								<th class="bg-body-secondary">Status</th>
								<th class="bg-body-secondary">Options</th>
							</tr>
						</thead>
						<tbody id="stList">

							<?php $students = getStudentsFromDB(); ?>

							<?php if (isset($fetchAllError)) { ?>
								<div class="server-message">
									<p><?php echo $fetchAllError; ?></p>
								</div>
							<?php } ?>

							<?php foreach ($students as $student) {
								$student['birthday'] = date('d.m.Y', strtotime($student['birthday'])) ?>

								<tr id="<?= $student["id"]; ?>" data-id="<?= $student["id"]; ?>" data-group="<?= $student["group_id"]; ?>" 
									data-first-name="<?= $student["first_name"]; ?>" data-last-name="<?= $student["last_name"]; ?>" 
									data-gender="<?= $student["gender_id"]; ?>" data-birthday="<?= $student["birthday"]; ?>" 
									data-status="<?= $student["status"] ? 'true' : 'false'; ?>">

									<td><input type='checkbox'></td>
									<td class='group'><?= $groupsArray[$student["group_id"]]; ?></td>
									<td class='name'><?= $student["first_name"] . " " . $student["last_name"]; ?></td>
									<td class='gender'><?= $gendersArray[$student["gender_id"]]; ?></td>
									<td class='birthday'><?= $student["birthday"]; ?></td>
									<td><span class='status-dot <?= $student["status"] ? "status-dot-active" : ""; ?>'></span></td>
									<td>
										<button class="action-button rounded-4" data-action-id="<?= $student["id"]; ?>" style="margin-right: 4px;">
											<i class="fa-solid fa-pencil"></i>
										</button>
										<button class="delete-button rounded-4" data-delete-id="<?= $student['id']; ?>">
											<i class="fa-solid fa-xmark"></i>
										</button>
									</td>
								</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
				<!-- Модальне вікно додавання/редагування -->
				<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="addModalLabel">Add student</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<form id="addForm" method="post" action="submitstudent.php">
									<input type="hidden" id="id">
									<div class="mb-3">
										<label for="group" class="form-label">Group:</label>
										<select id="group" class="form-select" aria-label="Default select example">
											<option selected disabled value="" data-group-id="0">Select group</option>
											<?php foreach ($groupsArray as $key => $group) { ?>
												<option value="<?= $key ?>" data-group-id="<?= $key ?>"><?= $group ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="mb-3">
										<label for="fname" class="form-label">First name:</label>
										<input type="text" class="form-control" id="fname" placeholder="Enter your first name">
									</div>
									<div class="mb-3">
										<label for="lname" class="form-label">Last name:</label>
										<input type="text" class="form-control" id="lname" placeholder="Enter your last name">
									</div>
									<div class="mb-3">
										<label for="gender" class="form-label">Gender:</label>
										<select id="gender" class="form-select" aria-label="Default select example">
											<option selected disabled value="" data-gender-id="0">Select gender</option>
											<?php foreach ($gendersArray as $key => $gender) { ?>
												<option value="<?= $key ?>" data-gender-id="<?= $key ?>"><?= $gender ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="mb-3">
										<label for="birthday" class="form-label">Birthday:</label>
										<input type="date" id="birthday" class="form-control" min="2000-01-01" max="2005-12-31" title="Select the birthday date from the drop-down calendar">
									</div>
									<div class="row mb-3">
										<div class="col-auto">
											<label class="form-check-label" for="statusSw">Status:</label>
										</div>
										<div class="col-auto form-check form-switch">
											<input class="form-check-input" type="checkbox" role="switch" id="statusSw">
										</div>
									</div>
									<div class="d-flex justify-content-end">
										<div class="divErrors container-fluid d-flex align-items-center border border-danger me-3 text-danger rounded-2 d-none"></div>
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
										<button type="submit" id="modalSubmitButton" class="btn btn-primary ms-3">Add</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<!-- Модальне вікно видалення -->
				<div class="modal fade" id="confirmDeleteModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="confirmDeleteModalLabel">Confirm Deletion</h5>
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body">
								<div id="delete-question"></div>
								<div class="delete-error container-fluid d-flex align-items-center border border-danger me-3 text-danger rounded-2 d-none"></div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
								<button type="button" class="btn btn-danger" id="confirmDeleteButton">Delete</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="script.js"></script>
	<!-- <script src="app.js"></script> -->
</body>

</html>