const socket = io('http://localhost:3000');
const userForm = document.getElementById('user-form');
const userFormContainer = document.getElementById('user-form-container');
const login = document.getElementById('login');
const chatView = document.getElementById('chat-view-container');
const roomsList = document.getElementById('rooms-list');
const messages = document.getElementById('messages');
const members = document.getElementById('members');
const sendForm = document.getElementById('send-form');
const messageInput = document.getElementById('message-input');

let userId = null;
let userName = null
let currentRoom = null;
let chatList = {};
let usersList = {};

login.focus();

userForm.addEventListener('submit', e => {
  e.preventDefault();

  userName = login.value;

  userFormContainer.classList.add('d-none');
  chatView.classList.remove('d-none');
  socket.emit('new-user', userName);
})

sendForm.addEventListener('submit', e => {
  e.preventDefault();
  const message = messageInput.value;

  AddSelfMessage(message);
  const data = { senderSocketId: userId, senderUserName: userName, receiverUserName: currentRoom, message: message };
  chatList[currentRoom].push({ senderUserName: userName, message: message });

  socket.emit('send-message', data);
  messageInput.value = '';
  messageInput.focus();
})

roomsList.addEventListener('click', function (event) {
  if (event.target.classList.contains('room')) {
    let id = event.target.getAttribute('data-id');
    let name = event.target.getAttribute('data-name');
    OpenRoom(id, name);
  }
});

socket.on('user-connect', data => {
  const adminChatElement = AddRoom(0, "Admin");
  adminChatElement.classList.add('bg-primary-subtle', 'rounded');

  roomsList.appendChild(adminChatElement);

  currentRoom = 0;
  chatList[0] = [];

  userId = data.id;

  usersList = data.usersList;

  for (let socketId in data.usersList) {
    AddMember(data.usersList[socketId], socketId);
    if (data.usersList[socketId] == userName) {
      continue;
    }

    chatList[data.usersList[socketId]] = [];
    const group = AddRoom(socketId, data.usersList[socketId]);
    roomsList.appendChild(group);
  }

  data.chatHistory.forEach(message => {
    chatList[0].push(message);
  });
  ShowChatMessages(chatList[0]);
});

socket.on('message', data => {
  let targetRoom = data.receiverUserName == 0 ? 0 : data.senderUserName;

  if(chatList[targetRoom].length !== 0){
    chatList[targetRoom].push(data);
  }
  if (currentRoom == targetRoom) {

    AddMessage(data.senderUserName, data.message);
  }
  else {
    AddUnreadDot(targetRoom);
  }
});

async function OpenRoom(id, name) {
  chatRoom = id == 0 ? 0 : name;
  if (currentRoom == chatRoom) {
    return;
  }

  currentRoom = chatRoom;

  const rooms = document.querySelectorAll('.room');
  rooms.forEach(room => {
    if (room.getAttribute('data-name') == name) {
      room.classList.add('bg-primary-subtle', 'rounded');
      const unreadDot = room.querySelector('.unread');
      if (unreadDot) {
        unreadDot.remove();
      }
    } else {
      room.classList.remove('bg-primary-subtle', 'rounded');
    }
  });

  document.querySelector('.chat-name').textContent = `Chat room ${name}`;
  messages.innerHTML = '';

  if (id == 0) {
    SetMembers(usersList);
  } else {
    SetMembers({ [id]: name, [userId]: userName });
  }

  if(chatList[currentRoom].length === 0){
    socket.emit('get-chat-history', currentRoom, userName);
  }
  else{
    ShowChatMessages(chatList[currentRoom]);
  }
}

function ShowChatMessages(messages) {
  if (!messages) return;
  messages.forEach(message => {
    if (message.senderUserName === userName) {
      AddSelfMessage(message.message);
    } else {
      AddMessage(message.senderUserName, message.message);
    }
  });
}

socket.on('chat-history', messages => {
  if (!messages) return; 
  messages.forEach(message => {
    chatList[currentRoom].push(message);
  });
  ShowChatMessages(messages);
});

socket.on("new-chat", data => {
  if (chatList[data.name]) {
    return;
  }
  chatList[data.name] = [];
  usersList[data.id] = data.name;
  let newGroup = AddRoom(data.id, data.name);
  roomsList.append(newGroup);

  if (currentRoom == 0) {
    AddMember(data.name, data.id);
  }
})

function AddRoom(id, name) {
  const group = document.createElement('div');
  group.classList.add('room', 'p-2', 'd-flex', 'justify-content-between', 'align-items-center');
  group.setAttribute('data-id', id);
  group.setAttribute('data-name', name);
  group.innerHTML =
    `
    <span>
      <span>
        <img src="sf.jfif" alt="avatar" class="rounded-circle" width="32">
      </span> 
      <span class="m-1">
        ${name}
      </span>
    </span>
  `;
  return group;
}

function AddUnreadDot(chatRoom) {
  const roomElement = document.querySelector(`div[data-name="${chatRoom}"], div[data-id="${chatRoom}"]`);
  if (roomElement) {
    const existingSpan = roomElement.querySelector('.unread');
    if (!existingSpan) {
      const unreadDot = document.createElement('span');
      unreadDot.classList.add('unread', 'd-inline-block', 'rounded-circle', 'bg-danger');
      roomElement.appendChild(unreadDot);
    }
  }
}

function SetMembers(membersList) {
  while (members.firstChild) {
    members.removeChild(members.firstChild);
  }
  for (let socketId in membersList) {
    AddMember(membersList[socketId], socketId);
  }
}

function AddMember(name, id) {
  const memberElement = document.createElement('div');
  memberElement.classList.add('d-flex', 'flex-column', 'justify-content-center', 'align-items-center', 'm-1');
  memberElement.setAttribute('data-id', id);
  memberElement.innerHTML =
    `
    <span>
      <img src="sf.jfif" alt="avatar" class="rounded-circle" width="32">
    </span> 
    <span>
      ${name}
    </span>
  `;
  members.appendChild(memberElement);
}

function AddMessage(senderUserName, message) {
  const messageElement = document.createElement('div');
  messageElement.classList.add('d-flex', 'flex-row', 'justify-content-start', 'align-items-center');
  messageElement.innerHTML =
    `
    <div class="d-flex flex-column justify-content-center align-items-center m-1">
      <span>
        <img src="sf.jfif" alt="avatar" class="rounded-circle" width="32">
      </span> 
      <span class="mt-1">
        ${senderUserName}
      </span>
    </div>
    <div class="messages-width p-2 bg-info-subtle rounded m-2">
      <span class="messages-break">${message}</span>
    </div>
  `;
  messages.appendChild(messageElement);
}

function AddSelfMessage(message) {
  const messageElement = document.createElement('div');
  messageElement.classList.add('d-flex', 'flex-row', 'align-items-center', 'justify-content-end', 'px-2');
  messageElement.innerHTML =
    `
    <div class="messages-width p-2 bg-warning-subtle rounded w-auto m-2">
      <span class="messages-break">${message}</span>
    </div>
    <div class="d-flex flex-column justify-content-center align-items-center m-1">
      <span>
        <img src="sf.jfif" alt="avatar" class="rounded-circle" width="32">
      </span> 
      <span>
        Me
      </span>
    </div>
  `;
  messages.appendChild(messageElement);
  messages.scrollTop = messages.scrollHeight;
}