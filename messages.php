<!DOCTYPE html>
<html lang="uk">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Messages</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
	<div class="container-fluid p-0 d-flex h-100 fw-bold">
		<div id="bdSidebar" class="d-flex flex-column flex-shrink-0 bg-secondary text-white offcanvas-md offcanvas-start text-center">
			<a href="#" class="navbar-brand bg-body-secondary text-black p-3">CMS</a>
			<ul class="mynav nav nav-pills flex-column mb-auto p-3">
				<li class="nav-item mb-1">
					<a href="tasks.php">
						Tasks
					</a>
				</li>
				<li class="nav-item mb-1">
					<a href="index.php">
						Students
					</a>
				</li>
				<li class="nav-item mb-1">
					<a href="dashboard.php">
						Dashboard
					</a>
				</li>
			</ul>
		</div>

		<div class="container-fluid p-0 bg-light">
			<div class="p-3 d-md-none d-flex text-black bg-body-secondary">
				<a href="#" class="text-black" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
					<i class="fa-solid fa-bars"></i>
				</a>
				<span class="ms-3">CMS</span>
			</div>

			<div class="row p-2 m-0 bg-body-secondary idk2">
				<div class="col d-flex align-middle justify-content-end">
					<li class="nav-item dropdown d-block me-2 idk">
						<a class="nav-link dropdown-toggle p-2" href="messages.php" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<span class="notifications">
								<i class="fa-regular fa-bell fa-xl"></i>
								<span class="dot"></span>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="messages.php"><i class="fa-regular fa-user"></i> Message #1
									<span class="user-text">user1</span></a></li>
							<li><a class="dropdown-item" href="#"><i class="fa-regular fa-user"></i> Message #2
									<span class="user-text">user2</span></a></li>
							<li><a class="dropdown-item" href="#"><i class="fa-regular fa-user"></i> Message #3
									<span class="user-text">user3</span></a></li>
						</ul>
					</li>
					<li class="nav-item dropdown d-block idk">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<img src="sf.jfif" class="user-avatar d-inline-block align-text-center">
							<span class="d-none d-sm-inline">Shadow Fiend</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Profile</a></li>
							<li><a class="dropdown-item" href="#">Log out</a></li>
						</ul>
					</li>
				</div>
			</div>

			<div class="container p-4 col-4" id="user-form-container">
				<form id="user-form">
					<div class="mb-3">
						<label for="login" class="form-label">Login</label>
						<input type="text" class="form-control" id="login" aria-describedby="loginHelp" required placeholder="Enter your login">
					</div>
					<button type="submit" class="btn btn-primary">Log in</button>
				</form>
			</div>

			<div class="container-fluid p-4 d-none" id="chat-view-container">
				<h1>Messages</h1>
				<div class="container-fluid d-flex" id="chat-container">
					<div class="col-md-4 rounded border border-secondary p-3 mt-3">
						<div class="d-flex justify-content-between align-items-center">
							<h5>Chat rooms</h5>
							<button type="button" class="btn mb-2">+ New chat room</button>
						</div>
						<div id="rooms-list" class="overflow-y-auto"></div>
					</div>
					<div class="col-md-8 rounded border border-secondary p-3 mt-3 d-flex flex-column ms-4">
						<div class="d-flex flex-column">
							<div class="chat-name mb-4">
								<span>Chat room Admin</span>
							</div>
							<div class="flex-grow-0 mb-2">
								<div class="mb-1">
									<span>Members</span>
								</div>
								<div id="members" class="d-flex flex-wrap">
								</div>
							</div>
						</div>
						<div class="mt-4">
							<span>Messages</span>
						</div>
						<div id="messages" class="d-flex flex-column flex-grow-1 overflow-y-auto border border-dark rounded-2 p-3"></div>
						<form id="send-form" class="mt-4 d-flex align-items-center">
							<input type="text" class="form-control message-textarea flex-grow-1 border border-secondary" id="message-input" placeholder="Enter message" required>
							<button type="submit" class="ms-2 btn btn-primary">Send</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.3.1/socket.io.js"></script>
	<script src="https://cdn.socket.io/4.3.2/socket.io.min.js"></script>
	<script defer src="/messages.js"></script>
</body>
</html>