<?php
include "config.php";
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['group'])) {
        confirmError(101, "Choose group!");
    } else if (!array_key_exists($_POST['group'], $groupsArray)) {
        confirmError(102, "Incorrect group!");
    }
    if (empty(trim($_POST['firstName']))) {
        confirmError(103, "Enter first name!");
    }
    if (strlen($_POST['firstName']) < 2) {
        confirmError(104, "Too short first name!");
    }
    if (strlen($_POST['firstName']) > 25) {
        confirmError(105, "Too long first name!");
    }
    if (empty(trim($_POST['lastName']))) {
        confirmError(106, "Enter last name!");
    }
    if (strlen($_POST['lastName']) < 2) {
        confirmError(107, "Too short last name!");
    }
    if (strlen($_POST['lastName']) > 25) {
        confirmError(108, "Too long last name!");
    }
    if (empty($_POST['gender'])) {
        confirmError(109, "Choose gender!");
    } else if (!array_key_exists($_POST['gender'], $gendersArray)) {
        confirmError(110, "Incorrect gender!");
    }
    if (empty($_POST['birthday']) || strtotime($_POST['birthday']) === false) {
        confirmError(111, "Choose birthday!");
    }

    $id = $_POST['id'];
    $group_id = $_POST['group'];
    $first_name = $_POST['firstName'];
    $last_name = $_POST['lastName'];
    $gender_id = $_POST['gender'];
    $birthday = date('Y-m-d', strtotime($_POST['birthday']));
    $status = $_POST['status'] === 'true';

    if ($id) {
        $stmt = $conn->prepare("UPDATE `studentstable` SET `group_id` = '$group_id', `first_name` = '$first_name', `last_name`= '$last_name',
            `gender_id` = '$gender_id', `birthday` = '$birthday', `status` = :stat WHERE `id` = :id");
    } else {
        $stmt = $conn->prepare("INSERT INTO `studentstable` (`group_id`, `first_name`, `last_name`, `gender_id`, `birthday`, `status`) 
            VALUES ('$group_id', '$first_name', '$last_name', '$gender_id', '$birthday', :stat)");
    }

    $stmt->bindParam(':stat', $status, PDO::PARAM_BOOL);

    try {
        if ($id === "") {
            $stmt->execute();
            $response["id"] = $conn->lastInsertId();
        } else {
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $response["id"] = $id;
        }
    } catch (PDOException $e) {
        confirmError($e->getCode(), $e->getMessage() . $status);
    }

    $response["status"] = true;
    echo json_encode($response);
    exit;
}

http_response_code(403);
echo "Forbidden resourse!";
