const modal = new bootstrap.Modal(document.getElementById('addModal'), {keyboard: false})
const modalForm = document.querySelector('#addForm');
const actionButtons = document.querySelectorAll('.action-button');
const submitModalButton = document.getElementById('modalSubmitButton');
const deleteButtons = document.querySelectorAll('.delete-button');
const confirmDelModal = new bootstrap.Modal(document.getElementById('confirmDeleteModal'), {keyboard: false})
const confirmDelButton = document.querySelector('#confirmDeleteButton');
const deleteError = document.querySelector('.delete-error');
const tableBody = document.getElementById('stList');
const divErrors = document.querySelector(".divErrors");

// change modal text
function ChangeForEdit() {
    const modalHeader = document.querySelector('#addModal .modal-header h5');
    const submitButton = document.querySelector('#addModal #modalSubmitButton');

    modalHeader.textContent = 'Edit student';
    submitButton.textContent = 'Save';
}

function ChangeForAdd() {
    const modalHeader = document.querySelector('#addModal .modal-header h5');
    const submitButton = document.querySelector('#addModal #modalSubmitButton');

    modalHeader.textContent = 'Add student';
    submitButton.textContent = 'Add';
}

// add/edit listeners
actionButtons.forEach(button => ActionListener(button));

function ActionListener(button) {
    button.addEventListener('click', function (event) {

        let dataObject = GetEmptyObject();
        const rowID = this.getAttribute('data-action-id');

        if (rowID) {
            dataObject.id = rowID;
            const row = document.getElementById(rowID);
            GetRowValues(dataObject, row);
            ChangeForEdit();
        }

        FillModalValues(dataObject);
        if (divErrors.classList.contains('d-block')) {
            divErrors.classList.remove('d-block');
            divErrors.classList.add('d-none');
        }
        modal.show();
    });
};

// submition listeners
if (submitModalButton !== null) {
    submitModalButton.addEventListener('click', async function (event) {
        event.preventDefault();
        const dataObject = getFormValues();

        SubmitRequest(dataObject).then(
            function (requestInfo) {
                if (requestInfo.status) {
                    dataObject.id = requestInfo.id;
                    AddEditRow(dataObject);
                    modal.hide();
                    ChangeForAdd();
                }
            }
        )
    });
}

// delete listeners
deleteButtons.forEach(button => DeleteListener(button));

function DeleteListener(button) {
    button.addEventListener('click', function () {
        const row = button.closest('tr');
        const fullName = row.querySelector('.name').textContent;
        const message = `Are you sure you want to delete user ${fullName}?`;
        const deleteQuestionText = document.querySelector('#delete-question');
        deleteQuestionText.textContent = message;
        confirmDelModal.show();
        confirmDelButton.dataset.rowID = row.id;
    });
}

// confirm delete listeners
if (confirmDelButton !== null) {
    confirmDelButton.addEventListener('click', function () {
        const rowID = this.dataset.rowID;
        const row = document.getElementById(rowID);
        if (row) {
            var dataObject = { id: rowID };
            DeleteRequest(dataObject).then(
                function (isStatus) {
                    if (isStatus) {
                        row.remove();
                        confirmDelModal.hide();
                    }
                }
            )
            if (deleteError.classList.contains('d-block')) {
                deleteError.classList.remove('d-block');
                deleteError.classList.add('d-none');
            }
        }
    });
}

// submit request
function SubmitRequest(data) {
    const encodedData = new URLSearchParams();
    for (const key in data) {
        encodedData.append(key, data[key]);
    }
    return fetch("submit-student.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: encodedData
    })
        .then(requestInfo => requestInfo.json())
        .then(result => {
            if (!result.status) {
                if (result.error && result.error.message) {
                    divErrors.classList.remove('d-none');
                    divErrors.classList.add('d-block');
                    divErrors.textContent = "Error " + result.error.code + ". " + result.error.message;
                }
            }
            return result;
        });
}

// delete request
function DeleteRequest(data) {
    const encodedData = new URLSearchParams();
    for (const key in data) {
        encodedData.append(key, data[key]);
    }

    return fetch("delete-student.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: encodedData
    })
        .then(requestInfo => requestInfo.json())
        .then(result => {
            if (!result.status) {
                if (result.error && result.error.message) {
                    deleteError.classList.remove('d-none');
                    deleteError.classList.add('d-block');
                    deleteError.textContent = result.error.message;
                }
            }
            return result.status;
        });
}

function GetRowValues(dataObject, row) {
    dataObject.group = row.getAttribute('data-group');
    dataObject.firstName = row.getAttribute('data-first-name');
    dataObject.lastName = row.getAttribute('data-last-name');
    dataObject.gender = row.getAttribute('data-gender');

    const birthday = row.getAttribute('data-birthday');
    const parts = birthday.split('.');
    dataObject.birthday = parts[2] + '-' + parts[1] + '-' + parts[0];
    dataObject.status = row.getAttribute('data-status') === "true";
}

function FillModalValues(dataObject) {
    modalForm.querySelector('#id').value = dataObject.id;
    modalForm.querySelector('#group').value = dataObject.group;
    modalForm.querySelector('#fname').value = dataObject.firstName;
    modalForm.querySelector('#lname').value = dataObject.lastName;
    modalForm.querySelector('#gender').value = dataObject.gender;
    modalForm.querySelector('#birthday').value = dataObject.birthday;
    modalForm.querySelector('#statusSw').checked = dataObject.status;
}

function AddEditRow(dataObject) {
    const newRow = AssignNewRow(dataObject);

    tableBody.appendChild(newRow);
    ActionListener(newRow.querySelector('.action-button'));
    DeleteListener(newRow.querySelector('.delete-button'));

    if (dataObject.id) {
        const row = document.getElementById(`${dataObject.id}`);
        if (row) {
            row.parentNode.replaceChild(newRow, row);
        }
    }

    if (Object.keys(dataObject).length !== 0) {
        const jsonStr = JSON.stringify(dataObject, null, 2);
        console.log(jsonStr);
    }
}

function AssignNewRow(dataObject) {
    const newRow = document.createElement('tr');

    newRow.id = dataObject.id;

    newRow.setAttribute('data-id', newRow.id);
    newRow.setAttribute('data-group', dataObject.group);
    newRow.setAttribute('data-first-name', dataObject.firstName);
    newRow.setAttribute('data-last-name', dataObject.lastName);
    newRow.setAttribute('data-gender', dataObject.gender);
    newRow.setAttribute('data-birthday', dataObject.birthday);
    newRow.setAttribute('data-status', dataObject.status);

    const status = dataObject.status ? 'status-dot-active' : '';

    const selectGroup = document.querySelector(`#group option[data-group-id="${dataObject.group}"]`);
    const group = selectGroup ? selectGroup.textContent : '';

    const selectGender = document.querySelector(`#gender option[data-gender-id="${dataObject.gender}"]`);
    const gender = selectGender ? selectGender.textContent : '';

    newRow.innerHTML = `
        <td><input type="checkbox"></td>
        <td class="group">${group}</td>
        <td class="name">${dataObject.firstName} ${dataObject.lastName}</td>
        <td class="gender">${gender}</td>
        <td class="birthday">${dataObject.birthday}</td>
        <td><span class="status-dot ${status}"></span></td>
        <td>
            <button class="action-button rounded-4" data-action-id="${newRow.id}" style="margin-right: 4px;">
                <i class="fa-solid fa-pencil"></i>
            </button>
            <button class="delete-button rounded-4">
                <i class="fa-solid fa-xmark"></i>
            </button>
        </td>
    `;

    return newRow;
}

function GetEmptyObject() {
    let object = {
        id: "",
        group: "",
        firstName: "",
        lastName: "",
        gender: "",
        birthday: "",
        status: false
    };
    return object;
}

function getFormValues() {
    let dataObject = GetEmptyObject();
    dataObject.id = modalForm.querySelector('#id').value;
    dataObject.group = modalForm.querySelector('#group').value;
    dataObject.firstName = modalForm.querySelector('#fname').value;
    dataObject.lastName = modalForm.querySelector('#lname').value;
    dataObject.gender = modalForm.querySelector('#gender').value;
    const birthday = modalForm.querySelector('#birthday').value;
    const parts = birthday.split('-');
    dataObject.birthday = parts[2] + '.' + parts[1] + '.' + parts[0];
    dataObject.status = modalForm.querySelector('#statusSw').checked ? true : false;
    return dataObject;
}