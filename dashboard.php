<!DOCTYPE html>
<html lang="ua">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>My CMS</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
</head>

<body>

	<div class="container-fluid p-0 d-flex h-100 fw-bold">
		<div id="bdSidebar" class="d-flex flex-column 
					flex-shrink-0 
					bg-secondary 
					text-white offcanvas-md offcanvas-start text-center">
			<a href="#" class="navbar-brand bg-body-secondary text-black p-3">CMS
			</a>
			<ul class="mynav nav nav-pills flex-column mb-auto p-3">
				<li class="nav-item mb-1">
					<a href="tasks.html">
						Tasks
					</a>
				</li>

				<li class="nav-item mb-1">
					<a href="index.html">
						Students
					</a>
				</li>
				<li class="nav-item mb-1">
					<a href="dashboard.html" class="active">
						Dashboard
					</a>
				</li>
			</ul>
		</div>

		<div class="container-fluid p-0 bg-light">
			<div class="p-3 d-md-none d-flex text-black bg-body-secondary">
				<a href="#" class="text-black" data-bs-toggle="offcanvas" data-bs-target="#bdSidebar">
					<i class="fa-solid fa-bars"></i>
				</a>
				<span class="ms-3">CMS</span>
			</div>

			<div class="row p-2 m-0 bg-body-secondary idk2">
				<div class="col d-flex align-middle justify-content-end">
					<li class="nav-item dropdown d-block me-2 idk">
						<a class="nav-link dropdown-toggle p-2" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<span class="notifications">
								<i class="fa-regular fa-bell fa-xl"></i>
								<span class="dot"></span>
							</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#"><i class="fa-regular fa-user"></i> Message #1
									<span class="user-text">user1</span></a></li>
							<li><a class="dropdown-item" href="#"><i class="fa-regular fa-user"></i> Message #2
									<span class="user-text">user2</span></a></li>
							<li><a class="dropdown-item" href="#"><i class="fa-regular fa-user"></i> Message #3
									<span class="user-text">user3</span></a></li>
						</ul>
					</li>
					<li class="nav-item dropdown d-block idk">
						<a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
							<img src="sf.jfif" class="user-avatar d-inline-block align-text-center">
							<span class="d-none d-sm-inline">Shadow Fiend</span>
						</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Profile</a></li>
							<li><a class="dropdown-item" href="#">Log out</a></li>
						</ul>
					</li>
				</div>
			</div>
			<h1>Dashboard</h1>

			<script src="script.js"></script>
</body>

</html>